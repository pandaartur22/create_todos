let todos = document.querySelector('#todo')
let completed = document.querySelector('#completed')

let AddTodoBtn = document.querySelector('#add')

let todoName = document.querySelector('.header-input')

let listTodo = []
let todoItem = document.querySelector('.todo-item')

document.addEventListener('DOMContentLoaded', () => {
  rendering()
})

function rendering() {
  todos.innerHTML = ''
  completed.innerHTML = ''

  listTodo.forEach(item => {

    let cloneTodoItem = todoItem.cloneNode(true)
    cloneTodoItem.firstElementChild.textContent = item.value

    let removeTodo = cloneTodoItem.querySelector('.todo-remove')
    let toggleTodo = cloneTodoItem.querySelector('.todo-complete')

    removeTodo.addEventListener('click', () => {
      removeItemTodo(item.id)
    })

    toggleTodo.addEventListener('click', () => {
      toggleComplitedItemTodo(item.id)
    })

    !item.completed ? todos.prepend(cloneTodoItem) : completed.prepend(cloneTodoItem)

  })
}

AddTodoBtn.addEventListener('click', (e) => {
  e.preventDefault()

  if (todoName.value.trim()) {
    listTodo.push({
      id: listTodo.length,
      value: todoName.value.trim(),
      completed: false
    })

    todoName.value = ''

    rendering()
  }
})

function removeItemTodo(idTodo) {
  listTodo = listTodo.filter(item => {
    return item.id !== idTodo
  })

  rendering()
}

function toggleComplitedItemTodo(idTodo) {
  listTodo.find(item => {
    if (item.id == idTodo) {
      item.completed = !item.completed
    }
  })

  rendering()
}
